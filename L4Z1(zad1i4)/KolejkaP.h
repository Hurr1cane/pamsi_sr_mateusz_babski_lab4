#pragma once
#include "Lista2.h"

template<class Key, class T>
class KolejkaP
{
private:

	struct Para {
		Key key;		//klucz
		T value;		//warto��
	};

	Lista2<Para> lista;	//kolejka priorytetowa zdefiniowana na li�cie dwukierunkowej

public:

	T removeMin();											//usuwa i zwraca element o najmniejszym kluczu
	T min();												//zwraca element o najmniejszym kluczu
	void insert(Key k, T x);								//dodaje element do kolejki

	int size() const { return lista.size(); }				//zwraca rozmiar kolejki
	bool isEmpty() const { return lista.size() == 0; }		//zwraca true je�li kolejka jest pusta
};

#include "KolejkaP.inl"
