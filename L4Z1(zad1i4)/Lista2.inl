#include "Lista2.h"
#include "myexception1.h"
#include "myexception2.h"
#include <iostream>

template<class T>
Lista2<T>::Lista2() :
	glowa(),
	ogon(),
	rozmiar(0)
{
}

template<class T>
void Lista2<T>::dodaj_koniec(T o)
{
	Wezel * nowy = new Wezel(o);
	if (!ogon)								//utworzenie pierwszego elementu
	{
		glowa = nowy;
		ogon = nowy;
	}
	else									//dodanie elementu na koniec listy
	{
		ogon->nastepnik = nowy;
		nowy->poprzednik = ogon;
		ogon = nowy;
	}
	++rozmiar;
}

template<class T>
void Lista2<T>::dodaj_poczatek(T o)
{
	Wezel * nowy = new Wezel(o);
	if (!glowa)								//utworzenie pierwszego elementu
	{
		glowa = nowy;
		ogon = nowy;
	}
	else									//dodanie elementu na pocz�tek listy
	{
		nowy->nastepnik = glowa;
		glowa->poprzednik = nowy;
		glowa = nowy;
	}
	++rozmiar;
}


template<class T>
bool Lista2<T>::usun(int index)
{
	if (rozmiar == 0)										//wyj�tek pustej listy
		throw emptyList;

	if (index < 0 || index > rozmiar - 1)					//wyj�tek niepoprawnego indeksu
		throw bounds;

	Wezel * wezel = glowa;									//iterowanie do elementu o zadanym indeksie
	for (int i = 0; i < index; ++i)
		wezel = wezel->nastepnik;

	if (index != 0 && index < rozmiar - 1)					//przepisanie wska�nik�w s�siaduj�cych element�w
	{
		wezel->poprzednik->nastepnik = wezel->nastepnik;
		wezel->nastepnik->poprzednik = wezel->poprzednik;
	}
	else if(index == 0)										//przypadek usuwanej g�owy
	{
		glowa->nastepnik->poprzednik == nullptr;
		glowa = glowa->nastepnik;
		if (rozmiar - 1 == 0)								//gdy rozmiar = 0
			ogon = glowa;
	}
	else													//przypadek usuwanego ogona
	{
		ogon->poprzednik->nastepnik == nullptr;
		ogon = ogon->poprzednik;
	}
	delete wezel;
	--rozmiar;
	return true;
}

template<class T>
T Lista2<T>::get(int index)
{
	if (rozmiar == 0)										//wyj�tek pustej listy
		throw emptyList;

	if (index < 0 || index > rozmiar - 1)					//wyj�tek niepoprawnego indeksu
		throw bounds;

	Wezel * wezel = glowa;
	for (int i = 0; i < index; ++i)							//iterowanie do elementu o zadanym indeksie
		wezel = wezel->nastepnik;

	return wezel->wartosc;
}

template<class T>
void Lista2<T>::clear()
{
	if (rozmiar == 0)										//wyj�tek pustej listy
		throw emptyList;

	Wezel * wezel = glowa;
	Wezel * nastepny = nullptr;
	for (int i = 0; i < rozmiar; ++i)						//usuwanie kolejnych element�w listy
	{
		nastepny = wezel->nastepnik;
		delete wezel;
		wezel = nastepny;
	}
	glowa = nullptr;
	ogon = nullptr;
	rozmiar = 0;
}

template<class T>
void Lista2<T>::show() const
{
	if (!rozmiar) return;
	Wezel * wezel = glowa;
	for (int i = 0; i < rozmiar; ++i)						//wy�wietlanie kolejnych element�w listy
	{
		std::cout << wezel->wartosc << " ";
		wezel = wezel->nastepnik;
	}
	std::cout << std::endl;
}

template<class T>
bool Lista2<T>::isEmpty()
{
	return false rozmiar == 0;
}
