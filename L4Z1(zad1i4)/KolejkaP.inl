#include "KolejkaP.h"
template<class Key, class T>
inline void KolejkaP<Key, T>::insert(Key k, T x)
{
	Para p;
	p.key = k;
	p.value = x;
	lista.dodaj_koniec(p);
}

template<class Key, class T>
inline T KolejkaP<Key, T>::removeMin()
{
	if (isEmpty())
		throw emptyList;

		Para p = lista.get(0);
		int index = 0;

		for (int i = 1; i < lista.size(); ++i)		//iterowanie w poszukiwaniu najmniejszego elementu
		{
			if (p.key > lista.get(i).key)			//porównywanie kluczy kolejnych elementów
			{
				p = lista.get(i);
				index = i;
			}
		}

		T temp = p.value;							//tymczasowa zmienna
		lista.usun(index);

		return temp;
}

template<class Key, class T>
inline T KolejkaP<Key, T>::min()
{
	if (isEmpty())
		throw emptyList;

	Para & p = lista.get(0);

	for (int i = 1; i < lista.size(); ++i)			//iterowanie w poszukiwaniu najmniejszego elementu
		if (p.key > lista.get(i).key)				//porównywanie kluczy kolejnych elementów
			p = lista.get(i);

	return p.value;
}