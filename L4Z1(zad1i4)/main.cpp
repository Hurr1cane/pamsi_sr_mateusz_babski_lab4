#include <iostream>
#include <string>
#include "KolejkaP.h"
#include "Lista2.h"

using namespace std;

int main()
{
	getchar();
	KolejkaP<int, std::string> k;
	cout << "Testowanie kolejki priorytetowej i sortowania przez wybieranie" << endl;
	cout << "Dodanie do kolejki priorytetowej kolejno (klucz;wartosc): 4;asd , 10;qwe , 1;zxc" << endl;

	k.insert(4, "asd");
	k.insert(10, "qwe");
	k.insert(1, "zxc");

	cout << endl << "Czy kolejka jest pusta? : " << k.isEmpty() << endl;
	cout << endl << "Rozmiar kolejki: " << k.size() << endl;

	cout << endl << "Zwrocenie wartosci o najmniejszym kluczu, zwraca: " << k.min() << endl;
	cout << "Zwrocenie wartosci o najmniejszym kluczu, zwraca: " << k.min() << endl;
	cout << endl << "Usuniecie wartosci o najmniejszym kluczu, zwraca: " << k.removeMin() << endl;
	cout << endl << "Zwrocenie wartosci o najmniejszym kluczu, zwraca: " << k.min() << endl;

	cout << "Dodanie do kolejki 4;asg" << endl;
	k.insert(4, "asg");

	cout << endl << "Zwrocenie wartosci o najmniejszym kluczu, zwraca: " << k.min() << endl;
	cout << endl << "Usuniecie wartosci o najmniejszym kluczu, zwraca: " << k.removeMin() << endl;

	cout << endl << "Czy kolejka jest pusta? : " << k.isEmpty() << endl;
	cout << endl << "Rozmiar kolejki: " << k.size() << endl;

	cout << endl << "Usuniecie wartosci o najmniejszym kluczu, zwraca: " << k.removeMin() << endl;
	cout << endl << "Usuniecie wartosci o najmniejszym kluczu, zwraca: " << k.removeMin() << endl;

	cout << endl << "Czy kolejka jest pusta? : " << k.isEmpty() << endl;
	cout << endl << "Rozmiar kolejki: " << k.size() << endl;

	try
	{
		cout << endl << "Zwrocenie wartosci o najmniejszym kluczu, zwraca: " << k.min() << endl;
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}

	try
	{
		cout << endl << "Usuniecie wartosci o najmniejszym kluczu, zwraca: " << k.removeMin() << endl;
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}

	cout << endl << "Stworzenie listy intow o wartosciach 5, 10, 432, 1, 0, 213, 32, 12, 87, 59, 213, 168" << endl;
	Lista2<int> l;
	KolejkaP<int, int> k1;
	l.dodaj_koniec(5);
	l.dodaj_koniec(10);
	l.dodaj_koniec(432);
	l.dodaj_koniec(1);
	l.dodaj_koniec(0);
	l.dodaj_koniec(213);
	l.dodaj_koniec(32);
	l.dodaj_koniec(12);
	l.dodaj_koniec(87);
	l.dodaj_koniec(59);
	l.dodaj_koniec(213);
	l.dodaj_koniec(168);


	KolejkaP<int, int> sort;
	
	cout << "Lista: " << endl;
	l.show();

	for (int i = 0; i < l.size(); ++i)
	{
		k1.insert(l.get(i), l.get(i));
	}
	l.clear();
	do
	{
		l.dodaj_koniec(k1.removeMin());
	} while (!k1.isEmpty());

	std::cout << "Posortowana lista: " << std::endl;
	l.show();

	getchar();

	return 0;
}