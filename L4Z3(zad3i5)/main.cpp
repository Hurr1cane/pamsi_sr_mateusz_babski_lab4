#include <iostream>
#include "DrzewoBin.h"

using namespace std;

int main()
{
	DrzewoBin<int> d;


	getchar();
	cout << "Ogolny przypadek drzewa" << endl;
	cout << "Dodanie do drzewa elementow jak ponizej" << endl;
	cout << "                10                      " << endl;
	cout << "           |         |                 " << endl;
	cout << "           5          15               " << endl;
	cout << "          | |        | |               " << endl;
	cout << "          2 7       13 17              " << endl << endl;
	d.dodaj(10);
	d.dodaj(15);
	d.dodaj(5);
	d.dodaj(2);
	d.dodaj(7);
	d.dodaj(13);
	d.dodaj(17);

	cout << "Przejscie drzewa preorder" << endl;
	d.show_pre();
	cout << endl;

	cout << "Przejscie drzewa inorder" << endl;
	d.show_in();
	cout << endl;

	cout << "Przejscie drzewa postorder" << endl;
	d.show_post();
	cout << endl << endl;

	cout << "Korzen: " << d.showR() << endl;
	cout << "Wysokosc drzewa: " << d.wysokosc() << endl << endl;

	cout << "Usuniecie dwoch minimalnych wartosci" << endl << endl;
	d.usunMin();
	d.usunMin();

	cout << "Korzen: " << d.showR() << endl;
	cout << "Wysokosc drzewa: " << d.wysokosc() << endl << endl;

	cout << "Wyswietl preorder" << endl;
	d.show_pre();
	cout << endl;

	cout << "Usuniecie dwoch maksymalnych wartosci" << endl << endl;
	d.usunMax();
	d.usunMax();

	cout << "Korzen: " << d.showR() << endl;
	cout << "Wysokosc drzewa: " << d.wysokosc() << endl << endl;
	
	cout << "Wyswietl preorder" << endl;
	d.show_pre();
	cout << endl << endl;

	cout << "Korzen: " << d.showR() << endl << endl;

	cout << "Usuniecie dwoch minimalnych wartosci" << endl << endl;
	d.usunMin();
	d.usunMin();

	cout << "Korzen: " << d.showR() << endl;
	cout << "Wysokosc drzewa: " << d.wysokosc() << endl << endl;

	cout << "Wyswietl preorder" << endl;
	d.show_pre();
	cout << endl << endl;

	cout << "Usuniecie maksymalnej  wartosci" << endl << endl;
	d.usunMax();

	cout << endl << "Wyswietl preorder" << endl;
	d.show_pre();
	cout << endl << endl;

	cout << "Usuniecie minimalnej wartosci" << endl << endl;
	d.usunMin();


	cout << "Korzen: ";
	try
	{
		cout << d.showR() << endl;
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	cout << endl;

	cout << "Wysokosc drzewa: ";
	try
	{
		 cout << d.wysokosc() << endl;
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	cout << endl;
	

	getchar();
	return 0;
}