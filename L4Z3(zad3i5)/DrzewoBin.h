#pragma once

template <class Typ>
class DrzewoBin
{
private:
	struct Wezel {
		Wezel * ojciec;
		Wezel * synL;			//zawiera alament mniejszy lub nullptr
		Wezel * synP;			//zawiera alament wi�kszy/r�wny lub nullptr
		Typ wartosc;

		Wezel() : synL(nullptr), synP(nullptr), ojciec(nullptr) {}
		Wezel(Typ nowy) : synL(nullptr), synP(nullptr), ojciec(nullptr), wartosc(nowy) {}
	};

	Wezel * korzen;

	int wysokosc(Wezel * wez);				//rekurencyjnie oblicza wysoko�� poddrzewa i zwraca j�
	bool usunMin(Wezel*);					//usuwa najmniejszy element poddrzewa
	bool usunMax(Wezel*);					//usuwa najwiekszy element poddrzewa
	void dodaj(Wezel*, Typ nowy);			//dodaje element do poddrzewadrzewa
	void show_pre(Wezel*) const;			//wyswietlenie poddrzewa preorder
	void show_in(Wezel*) const;				//wyswietlenie poddrzewa inorder
	void show_post(Wezel*) const;			//wyswietlenie poddrzewa postorder
public:
	DrzewoBin();
	void dodaj(Typ nowy);		//dodaje element do drzewa
	bool usunMin();				//usuwa najmniejszy element drzewa
	bool usunMax();				//usuwa najwiekszy element drzewa
	Typ showR() const;			//zwraca warto�� przypisan� do korzenia
	int wysokosc();				//zwraca wysoko�� drzewa
	void show_pre() const;		//wyswietlenie drzewa preorder
	void show_in() const;		//wyswietlenie drzewa inorder
	void show_post() const;		//wyswietlenie drzewa postorder
};


#include "DrzewoBin.inl"