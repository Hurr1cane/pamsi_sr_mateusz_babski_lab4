#include "DrzewoBin.h"
#include "myexception3.h"

template<class Typ>
DrzewoBin<Typ>::DrzewoBin():
korzen(nullptr)
{
}

template<class Typ>
int DrzewoBin<Typ>::wysokosc()
{
	return wysokosc(korzen);
}

template<class Typ>
void DrzewoBin<Typ>::dodaj(Typ o)
{
	dodaj(korzen, o);
}

template<class Typ>
bool DrzewoBin<Typ>::usunMin()
{
	return usunMin(korzen);
}

template<class Typ>
bool DrzewoBin<Typ>::usunMax()
{
	return usunMax(korzen);
}

template<class Typ>
void DrzewoBin<Typ>::dodaj(Wezel * bufK, Typ o)
{
	if (korzen == nullptr)				//utworzenie korzenia
	{
		Wezel * nowy = new Wezel(o);
		korzen = nowy;
		bufK = korzen;
	}
	else if (o < bufK->wartosc)			//gdy dodawana warto�� jest mniejsza od warto�ci tymczasowego korzenia
	{
		if (bufK->synL != nullptr)		//wywo�anie rekurencyjne gdy warto�� bufora nie jest najmniejsza
		{
			dodaj(bufK->synL, o);
		}
		else
		{
			Wezel * nowy = new Wezel(o);
			bufK->synL = nowy;
			bufK->synL->ojciec = bufK;
			bufK = korzen;
		}
	}
	else								//gdy dodawana warto�� jest wi�ksza/r�wna warto�ci tymczasowego korzenia
	{
		if (bufK->synP != nullptr)		//wywo�anie rekurencyjne gdy warto�� bufora nie jest najwi�ksza
		{
			dodaj(bufK->synP, o);
		}
		else
		{
			Wezel * nowy = new Wezel(o);
			bufK->synP = nowy;
			bufK->synP->ojciec = bufK;
		}
	}
}

template<class Typ>
bool DrzewoBin<Typ>::usunMin(Wezel * bufK)
{
	if (korzen == nullptr)								//dla pustego drzewa
		return false;
	if (bufK->synL == nullptr)
	{
		if (bufK == korzen && bufK->synP != nullptr)	//korzen jest najmniejszym elementem i posiada syn�w
		{
			bufK->synP->ojciec = nullptr;
			korzen = bufK->synP;
		}
		else if (bufK == korzen)						//korzen jest jedynym elementem i nie posiada syn�w
		{
			korzen = nullptr;
		}
		else if (bufK->synP != nullptr)					//bufor jest najmniejszym elementem i posiada syn�w
		{
			bufK->synP->ojciec = bufK->ojciec;
			bufK->ojciec->synL = bufK->synP;
		}
		else											//bufor jest najmniejszym elementem i nie posiada syn�w
			bufK->ojciec->synL = nullptr;
		delete bufK;
		return true;
	}
	else
	{
		return usunMin(bufK->synL);						//wywo�anie rekurencyjne gdy bufor nie jest najmniejszym elementem
	}
}

template<class Typ>
bool DrzewoBin<Typ>::usunMax(Wezel * bufK)
{
	if (korzen == nullptr)								//dla pustego drzewa
		return false;
	if (bufK->synP == nullptr)
	{
		if (bufK == korzen && bufK->synL != nullptr)	//korzen jest najwi�kszym elementem i posiada syn�w
		{
			bufK->synL->ojciec = nullptr;
			korzen = bufK->synL;
		}
		else if (bufK == korzen)						//korzen jest jedynym elementem i nie posiada syn�w
		{
			korzen = nullptr;
		}
		else if (bufK->synL != nullptr)					//bufor jest najwi�kszym elementem i posiada syn�w
		{
			bufK->synL->ojciec = bufK->ojciec;
			bufK->ojciec->synP = bufK->synL;
		}
		else											//bufor jest najwi�kszym elementem i nie posiada syn�w
			bufK->ojciec->synP = nullptr;
		delete bufK;
		return true;
	}
	else
	{
		return usunMax(bufK->synP);						//wywo�anie rekurencyjne gdy bufor nie jest najwi�kszym elementem
	}
}

template<class Typ>
Typ DrzewoBin<Typ>::showR() const
{
	if (korzen == nullptr)								//wyj�tek pustego drzewa
		throw emptyT;
	else
		return korzen->wartosc;
}

template<class Typ>
int DrzewoBin<Typ>::wysokosc(Wezel * wez)
{
	if (korzen == nullptr)									//wyj�tek pustego drzewa
		throw emptyT;
	int lh = (wez->synL) ? wysokosc(wez->synL) + 1 : 0;		//rekurencyjne wyznaczenie wysoko�ci lewego poddrzewa
	int lp = (wez->synP) ? wysokosc(wez->synP) + 1 : 0;		//rekurencyjne wyznaczenie wysoko�ci prawego poddrzewa
	return (lh>lp)?lh:lp;									//por�wnanie wysoko�ci lewego i prawego poddrzewa i zwr�cenie wi�kszej z nich
}


template<class Typ>
inline void DrzewoBin<Typ>::show_pre() const
{
	if (!korzen)
		return;

	show_pre(korzen);
	std::cout << std::endl;
}

template<class Typ>
inline void DrzewoBin<Typ>::show_post() const
{
	if (!korzen)
		return;

	show_post(korzen);
	std::cout << std::endl;
}

template<class Typ>
inline void DrzewoBin<Typ>::show_in() const
{
	if (!korzen)
		return;

	show_in(korzen);
	std::cout << std::endl;
}

template<class Typ>
inline void DrzewoBin<Typ>::show_pre(Wezel * w) const							//pre-order
{
	std::cout << w->wartosc << " ";
	if (w->synL)
		show_pre(w->synL);
	if (w->synP)
		show_pre(w->synP);
}

template<class Typ>
inline void DrzewoBin<Typ>::show_post(Wezel * w) const							//post-order
{
	if (w->synL)
		show_post(w->synL);
	if (w->synP)
		show_post(w->synP);
	std::cout << w->wartosc << " ";
}

template<class Typ>
inline void DrzewoBin<Typ>::show_in(Wezel * w) const							//in-order
{
	if (w->synL)
		show_in(w->synL);
	std::cout << w->wartosc << " ";
	if (w->synP)
		show_in(w->synP);
}