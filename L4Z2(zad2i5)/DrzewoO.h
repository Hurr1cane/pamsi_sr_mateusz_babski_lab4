#pragma once
#include "Lista2.h"

template <class Typ>
class DrzewoO
{
private:
	struct Wezel {
		Lista2<Wezel *> syn;  //lista syn�w
		Typ wartosc;

		Wezel() : {}
		Wezel(Typ nowy) : wartosc(nowy) {}
		Typ getWartosc() const { return wartosc; }
		void setWartosc(Typ w) { wartosc = w}
		Wezel& getSyn(int x) { return syn.get(x); }
	};

	Wezel * korzen;

	int wysokosc(Wezel * wez);				//rekurencyjnie oblicza wysoko�� poddrzewa i zwraca j�
	void dodaj(Wezel *, Lista2<int>, Typ);	//dodaje element do poddrzewa
	bool usun(Wezel *, Lista2<int>);		//usuwa element poddrzewa
	void show_pre(Wezel * w) const;			//wyswietlenie poddrzewa preorder
	void show_in(Wezel * w) const;			//wyswietlenie poddrzewa inorder
	void show_post(Wezel * w) const;		//wyswietlenie poddrzewa postorder
public:
	DrzewoO();
	void dodaj(Lista2<int>, Typ);			//dodaje element do drzewa
	bool usun(Lista2<int>);					//usuwa najmniejszy element drzewa
	Typ showR() const;						//zwraca warto�� przypisan� do korzenia
	int wysokosc();							//zwraca wysoko�� drzewa
	void show_pre() const;					//wyswietlenie drzewa preorder
	void show_in() const;					//wyswietlenie drzewa inorder
	void show_post() const;					//wyswietlenie drzewa postorder

	Wezel * getKorzen() { return korzen; }
};


#include "DrzewoO.inl"