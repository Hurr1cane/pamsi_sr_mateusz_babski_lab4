#pragma once

template<class T>
class Lista2 {
private:
	struct Wezel {
		Wezel * nastepnik;
		Wezel * poprzednik;
		T wartosc;

		Wezel() : nastepnik(nullptr), poprzednik(nullptr) {}
		Wezel(T nowy) : nastepnik(nullptr), poprzednik(nullptr), wartosc(nowy) {}
	};

	Wezel * glowa;
	Wezel * ogon;
	int rozmiar;

public:
	Lista2();
	void dodaj_koniec(T nowy);				//dodaje element na ko�cu listy
	void dodaj_poczatek(T nowy);			//dodaje element na pocz�tku listy
	bool usun(int index);					//usun element o podanym indeksie
	T get(int index);						//zwr�� element o podanym indeksie
	void clear();							//wyczy�� list�
	void show() const;						//wy�wietl list�
	int size() const { return rozmiar; }	//zwr�� rozmiar/ilosc element�w listy
	bool isEmpty();							//zwr�� true je�li lista jest pusta
};

#include "Lista2.inl"
