#include <iostream>
#include <string>
#include "DrzewoO.h"
#include "Lista2.h"

using namespace std;

int main()
{
	DrzewoO<string> d;
	Lista2<int> l1;
	Lista2<int> l2;
	Lista2<int> l3;
	//Lista2<int> l13;
	//Lista2<int> l2;
	//l2.dodaj_koniec(0);
	//Lista2<int> l3;
	//l3.dodaj_koniec(0);
	//Lista2<int> l4;
	//l4.dodaj_koniec(0);
	//Lista2<int> l5;
	//l4.dodaj_koniec(1);
	//Lista2<int> l6;
	//l4.dodaj_koniec(1);
	//Lista2<int> l7;
	//l4.dodaj_koniec(1);
	//Lista2<int> l8;
	//l4.dodaj_koniec(2);
	//Lista2<int> l9;
	//l4.dodaj_koniec(2);
	//Lista2<int> l10;
	//l4.dodaj_koniec(2);

	cout << "Ogolny przypadek drzewa" << endl;
	cout << "Dodanie do drzewa elementow jak ponizej" << endl;
	cout << "                   1                    " << endl;
	cout << "           |       |       |            " << endl;
	cout << "          2        3        4           " << endl;
	cout << "        | | |    | | |    | | |         " << endl;
	cout << "        5 6 7    8 9 10  11 12 13       " << endl << endl;

	try
	{
		d.dodaj(l1, "1");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	try
	{
		d.dodaj(l1, "2");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	l1.dodaj_koniec(0);
	try
	{
		d.dodaj(l2, "3");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	l2.dodaj_koniec(0);
	try
	{
		d.dodaj(l3, "4");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	l3.dodaj_koniec(0);
	try
	{
		d.dodaj(l1, "5");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	l1.dodaj_koniec(1);
	try
	{
		d.dodaj(l2, "6");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	l2.dodaj_koniec(1);
	try
	{
		d.dodaj(l3, "7");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	l3.dodaj_koniec(1);
	try
	{
		d.dodaj(l1, "8");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	l1.dodaj_koniec(2);
	try
	{
		d.dodaj(l2, "9");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	l2.dodaj_koniec(2);
	try
	{
		d.dodaj(l3, "10");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	l3.dodaj_koniec(2);
	try
	{
		d.dodaj(l1, "11");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	try
	{
		d.dodaj(l2, "12");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}
	try
	{
		d.dodaj(l3, "13");
	}
	catch (std::exception & exe)
	{
		std::cout << exe.what() << std::endl;
	}

	cout << "Przejscie drzewa preorder" << endl;
	d.show_pre();
	cout << endl;

	cout << "Przejscie drzewa inorder" << endl;
	d.show_in();
	cout << endl;

	cout << "Przejscie drzewa postorder" << endl;
	d.show_post();
	cout << endl;

	getchar();
	return 0;
}