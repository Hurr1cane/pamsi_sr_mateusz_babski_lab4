#include <iostream>
#include <string>
#include "DrzewoO.h"
#include "Lista2.h"

using namespace std;

int main()
{
	DrzewoO <string> drzewo;

	int opcja;

	do {
		cout << "1.Dodaj element (na koniec listy synow)" << endl;
		cout << "2.Usun element" << endl;
		cout << "3.Wyswietl wartosc korzenia" << endl;
		cout << "4.Wyswietl wysokosc drzewa" << endl;
		cout << "5.Wyswietl drzewo pre-order" << endl;
		cout << "6.Wyswietl drzewo in-order" << endl;
		cout << "7.Wyswietl drzewo post-order" << endl;
		cout << "0.Koniec" << endl;
		cin >> opcja;
		switch (opcja)
		{
		case 0: break;
		case 1:
		{
			string x;
			Lista2 <int> index;
			int opcja1;
			cout << endl << "Podaj wartosc do dodania" << endl;
			cin >> x;
			cout << endl << "Tworzenie listy indeksow" << endl;
			cout << "Kazdy kolejny indeks oznacza dodanie elementu dla syna o podanym indeksie liczac od poczatku listy synow." << endl;
			cout << "Dla pustego drzewa dodany zostanie korzen" << endl;
			cout << "Dla pustej listy dodany zostanie syn korzenia" << endl;
			cout << "Przy podaniu niepoprawnego indeksu nastepuje wyjatek i przerwanie dzialania funkcji dodawania." << endl << endl;
			do {
				cout << "1.Dodaj kolejny indeks" << endl;
				cout << "0.Zakoncz dodawanie indeksow" << endl;
				cin >> opcja1;
				switch (opcja1)
				{
				case 0: break;
				case 1:
				{
					int ind;
					cout << "Podaj wartosc indeksu" << endl;
					cin >> ind;
					index.dodaj_koniec(ind);

					break;
				}
				default:
					cout << endl << "Nieprawidlowe wywolanie" << endl << endl;
					break;
				}
			} while (opcja1 != 0);

			try
			{
				drzewo.dodaj(index, x);
			}
			catch (std::exception & exe)
			{
				std::cout << exe.what() << std::endl;
			}

			break;
		}
		case 2:
		{
			Lista2 <int> index;
			int opcja1;
			cout << endl << "Tworzenie listy indeksow" << endl;
			cout << "Kazdy kolejny indeks ozncza usuniecie elementu dla syna o podanym indeksie liczac od poczatku listy synow." << endl;
			cout << "Przy podaniu niepoprawnego indeksu nastepuje wyjatek i przerwanie dzialania funkcji usuwania." << endl << endl;
			do {
				cout << "1.Dodaj kolejny indeks" << endl;
				cout << "0.Zakoncz dodawanie indeksow" << endl;
				cin >> opcja1;
				switch (opcja1)
				{
				case 0: break;
				case 1:
				{
					int ind;
					cout << "Podaj warto�� indeksu" << endl;
					cin >> ind;
					index.dodaj_koniec(ind);
					break;
				}
				default:
					cout << endl << "Nieprawidlowe wywolanie" << endl << endl;
					break;
				}
			} while (opcja1 != 0);

			try
			{
				drzewo.usun(index);
			}
			catch (std::exception & exe)
			{
				std::cout << exe.what() << std::endl;
			}
			break;
		}
		case 3:
		{
			try
			{
				cout << "Wartosc korzenia: " << drzewo.showR() << endl;
			}
			catch (std::exception & exe)
			{
				std::cout << exe.what() << std::endl;
			}
			break;
		}
		case 4:
		{
			try
			{
				cout << endl << "Wysokosc drzewa : " << drzewo.wysokosc() << endl;
			}
			catch (std::exception & exe)
			{
				std::cout << exe.what() << std::endl;
			}
			break;
		}

		case 5:
		{
			drzewo.show_pre();
			break;
		}

		case 6:
		{
			drzewo.show_in();
			break;
		}

		case 7:
		{
			drzewo.show_post();
			break;
		}

		default:
			cout << endl << "Nieprawidlowe wywolanie" << endl << endl;
			break;
		}
	} while (opcja != 0);

	return 0;
}
