#include "DrzewoO.h"
#include "Lista2.h"
#include "myexception3.h"

template<class Typ>
DrzewoO<Typ>::DrzewoO():
korzen(nullptr)
{
}

template<class Typ>
void DrzewoO<Typ>::dodaj(Lista2<int> index, Typ wart)
{
	dodaj(korzen, index, wart);
}

template<class Typ>
bool DrzewoO<Typ>::usun(Lista2<int> index)
{
	return usun(korzen, index);
}

template<class Typ>
int DrzewoO<Typ>::wysokosc()
{
	return wysokosc(korzen);
}

template<class Typ>
inline void DrzewoO<Typ>::show_pre() const
{
	if (!korzen)
		return;

	show_pre(korzen);
	std::cout << std::endl;
}

template<class Typ>
inline void DrzewoO<Typ>::show_post() const
{
	if (!korzen)
		return;

	show_post(korzen);
	std::cout << std::endl;
}

template<class Typ>
inline void DrzewoO<Typ>::show_in() const
{
	if (!korzen)
		return;

	show_in(korzen);
	std::cout << std::endl;
}

template<class Typ>
inline void DrzewoO<Typ>::show_pre(Wezel * w) const								//pre-order
{
	std::cout << w->wartosc << " ";
	for (int i = 0; i < w->syn.size(); ++i)
		show_pre(w->syn.get(i));
}

template<class Typ>
inline void DrzewoO<Typ>::show_post(Wezel * w) const							//post-order
{
	for (int i = 0; i < w->syn.size(); ++i)
		show_post(w->syn.get(i));
	std::cout << w->wartosc << " ";
}

template<class Typ>
inline void DrzewoO<Typ>::show_in(Wezel * w) const								//in-order
{
	for (int i = 0; i < (w->syn.size()) / 2; ++i)
		show_in(w->syn.get(i));
	std::cout << w->wartosc << " ";
	for (int i = w->syn.size() / 2; i < w->syn.size(); ++i)
		show_in(w->syn.get(i));
}

template<class Typ>
void DrzewoO<Typ>::dodaj(Wezel * bufK, Lista2<int> index, Typ wart)
{
	if (korzen == nullptr)													//utworzenie korzenia
	{
		Wezel * nowy = new Wezel(wart);
		korzen = nowy;
	}
	else
	{
		if (index.isEmpty())												//gdy lista indeks�w kolejnych syn�w jest pusta dodaj na koniec listy syn�w
		{
			Wezel * nowy = new Wezel(wart);
			bufK->syn.dodaj_koniec(nowy);
		}
		else																//wywo�anie rekurencyjne dla kolejnego syna z listy indeks�w
		{
			if (index.get(0) < 0 || index.get(0) > bufK->syn.size() - 1)	//wyj�tek niepoprawnego indeksu
				throw bounds;
			int x = index.get(0);
			index.usun(0);
			dodaj(bufK->syn.get(x), index, wart);
		}
	}
}

template<class Typ>
bool DrzewoO<Typ>::usun(Wezel * bufK, Lista2<int> index)  
{
	if (korzen == nullptr)													//dla pustego drzewa
		return false;
	if(wysokosc(bufK) < index.size())
		throw bounds;
	else 
	{
		if (index.size() == 1)												//gdy lista indeks�w kolejnych syn�w zawiera 1 element usun syna dla pozosta�ego indeksu
		{
			if (index.get(0) < 0 || index.get(0) > bufK->syn.size() - 1)	//wyj�tek niepoprawnego indeksu
				throw bounds;

			int x = index.get(0);

			if (bufK->syn.isEmpty())										//usuwane drzewo nie ma syn�w
				bufK->syn.usun(x);
			else															//usuwane drzewo ma syn�w
			{
				Lista2<Wezel *> tempL = bufK->syn.get(x)->syn;
				if (!tempL.isEmpty())
				{
					bufK->syn.dodaj_koniec(tempL.get(tempL.size() - 1));
					for (int i = tempL.size() - 2; i > -1; --i)
						bufK->syn.get(bufK->syn.size() - 1)->syn.dodaj_poczatek(tempL.get(i));
				}
				bufK->syn.usun(x);
			}
		}
		else if (index.size() == 0)											//usuwanie korzenia
		{
			if (korzen->syn.isEmpty())										//gdy korzen nie ma syn�w
			{
				delete korzen;
				korzen = nullptr;
			}
			else															//gdy korzen ma syn�w
			{
				for (int i = korzen->syn.size() - 2; i > -1; --i)
					korzen->syn.get(korzen->syn.size() - 1)->syn.dodaj_poczatek(korzen->syn.get(i));
				Wezel * tempW = korzen->syn.get(korzen->syn.size() - 1);
				delete korzen;
				korzen = tempW;
			}
		}
		else
		{
			if (index.get(0) < 0 || index.get(0) > bufK->syn.size() - 1)	//wyj�tek niepoprawnego indeksu
				throw bounds;

			int x = index.get(0);
			index.usun(0);
			usun(bufK->syn.get(x), index);
		}
	}

	return true;
}

template<class Typ>
Typ DrzewoO<Typ>::showR() const
{
	if (korzen == nullptr)														//wyj�tek pustego drzewa
		throw emptyT;
	else
		return korzen->wartosc;
}

template<class Typ>
int DrzewoO<Typ>::wysokosc(Wezel * wez)
{
	if (wez == nullptr)
		throw emptyT;

	int h = 0;

	for (int i = 0; i < wez->syn.size(); ++i)
	{
		int ah = ((wez->syn.get(i)) ? wysokosc(wez->syn.get(i)) + 1 : 1);		//rekurencyjne wyznaczenie wysoko�ci kolejnych poddrzew
		if (ah > h)
			h = ah;
	}
	return h;																	//zwr�cenie wysoko�ci najwi�kszego poddrzewa
}
 